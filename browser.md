#Browser Rendering 

![](https://www.google.com/search?q=browser+rendering+official+pic&client=ubuntu&hs=Mav&channel=fs&sxsrf=ALiCzsYoc3ZTW5DkBzGGIOXdycF8oEjEpw:1656744609877&source=lnms&tbm=isch&sa=X&ved=2ahUKEwjk3dHPztn4AhWT4TgGHYPbDkkQ_AUoAXoECAEQAw&biw=1853&bih=939&dpr=1#imgrc=o10HHzDSV1nCyM)


## Abstracts:
---
- In this article, we will deep dive into DOM and CSSOM to understand how the browser renders a webpage. The browser blocks some rendering of a webpage until certain resources are loaded first while other resources are loaded asynchronously.



## Table of Contents :
---

1. [Introduction](#introduction)

2. [Why browser rendering](#Why browser rendering?)

3. [Advantages Of Browser Rendering](#Advantages of browser rendering)

4. [Steps To Browser Rendering](#Steps to browser rendering)

5. [Conclusions](#conclusions)


---
## Introduction 
---
 - Rendering is a process used in web development that turns website code into the interactive pages users see when they visit a website. 

 - The term generally refers to the use of HTML, CSS, and JavaScript codes. 

 - The process is completed by a rendering engine, the software used by a web browser to render a web page.


---
##[Why Browser Rendering]
---

 - Software that renders HTML pages (Web pages). It turns the HTML layout tags in the page into the appropriate commands for the operating system, which causes the formation of the text characters and images for screen and printer.

 - Also called a "layout engine," a rendering engine is used by a Web browser to render HTML pages, by mail programs that render HTML email messages, as well as any other application that needs to render Web page content.

 - For example, Trident is the rendering engine for Microsoft's Internet Explorer, and Gecko is the engine in Firefox. Trident and Gecko are also incorporated into other browsers and applications.

  - sample of browsers and rendering engines. See HTML and render. RenderingBrowser Engine Source Internet Explorer, Trident Microsoft AOL Explorer , Trident Microsoft Firefox , Gecko Mozilla Netscape,  Gecko Mozilla Safari WebKit,  WebKit Chrome , WebKit WebKit Opera , Presto Opera Konqueror KHTML KHTML.


---
##[Advantages Of Browser Rendering]
---
 - Improved data security and PIPA compliance.

 - Improve page load time with minimized network latency.

 - Predictable server-side processing performance.

 - Accurate user metrics.

 - Fewer browser compatibility issues.


---
##[Steps To Browser Rendering]
---
 - First  we need to understand what DOM is. When a browser sends a request to a server to fetch an HTML document, the server returns an HTML page in binary stream format which is basically a text file with the response header Content-Type set to the value text/html; charset=UTF-8.

 - Here text/html is a MIME Type which tells the browser that it is an HTML document and charset=UTF-8 tells the browser that it is encoded in UTF-8 character encoding. Using this information, the browser can convert the binary format into a readable text file.

 - If this header is missing, the browser would not understand how to process the file and it will render in plain text format. But if everything is OK, after this conversion, the browser can start reading the HTML document. A typical HTML document could look like this.these is shown in below link.

![](https://gist.githubusercontent.com/thatisuday/be3e05400fed2c0749a8818e0f44f57a/raw/91a25f3ed09e1df8160183a489b9b1db53269758/index.html)

 - In the above link, our webpage is dependent on style.css to provide styles to HTML elements and main.js to perform some JavaScript operations. With some neat CSS styles.

#CSS
 - When we design a website, our intentions are to make it as good looking as possible. And we do that by providing some styles to HTML elements. 

 - In the HTML page, we provide styles to HTML elements using CSS which stands for Cascading Style Sheets. Using CSS selectors, we can target DOM elements and set a value to style property such as color or font-size.

 - There are different methods of applying styles to HTML elements like using an external CSS file, with embedded CSS using style tag, with an inline method using the style attribute on HTML elements or using JavaScript. But in the end, the browser has to do the heavy lifting of applying CSS styles to the DOM elements.

 - Let’s say, for our earlier example, we are going to use the below CSS styles. For the sake of simplicity, we are not going to be bothered about how we are importing the CSS styles in the HTML page.

![](https://gist.githubusercontent.com/thatisuday/712a3fd1301ff31a316b9e59c69723f9/raw/dd005e164f8368e2f5bae778dc0ae802a013ed87/styles.css)

 - After constructing the DOM, the browser reads CSS from all the sources (external, embedded, inline, user-agent, etc.) and construct a CSSOM. CSSOM stands for CSS Object Model which is a Tree Like structure just like DOM.

Each node in this tree contains CSS style information that will be applied to DOM elements that it target (specified by the selector). CSSOM, however, does not contain DOM elements which can’t be printed on the screen like meta, script, title etc.

 
 - Render tree is also a tree-like structure constructed by combining DOM and CSSOM trees together. The browser has to calculate the layout of each visible element and paint them on the screen  that browser uses this Render-Tree. Hence, unless Render-Tree isn’t constructed, nothing is going to get printed on the screen which is why we need both DOM and CSSOM trees.

 -  As Render-Tree is a low-level representation of what will eventually get printed on the screen, it won’t contain nodes that do not hold any area in the pixel matrix. For example, display:none; elements have dimensions of 0px X 0px, hence they won’t be present in Render-Tree.


---
##[Conclusions]
---
 - The rendering process explains how the code we write is finally displayed on the screen by the browser. The prior knowledge can help the developers to write more optimized code for a better-performing web page.


---
##References :
---
[Mdn plus](https://developer.mozilla.org/en-US/docs/Web/Performance/How_browsers_work)
[Log Rocket](https://blog.logrocket.com/how-browser-rendering-works-behind-scenes/)

